
var exec = require('cordova/exec');
var channel = require('cordova/channel');


module.exports.coolMethod = function (arg0, success, error) {
    exec(success, error, 'cacheDataSync', 'coolMethod', [arg0]);
};

let getChuckNorrisFact = (url) => {
    return new Promise(
        (resolve, reject) => {
            request.get(url, function(error, response, data){
                if (error) reject(error);

                let content = JSON.parse(data);
                let fact = content.value;
                resolve(fact);
            })
        }
    );
};

// var subscriptionResult = function () {};


module.exports.start = function (arg0, success, error) {
    console.log('... cordova plugin sync data : start ....');
    SyncPlugin.subscribe({});
    exec(success, error, 'cacheDataSync', 'start_sync', [arg0]);
};

module.exports.lockCache = function (arg0, success, error) {
    console.log('... cordova plugin lock cache : start ....');
    exec(success, error, 'cacheDataSync', 'lock_cache', [arg0]);
};

module.exports.unlockCache = function (arg0, success, error) {
    console.log('... cordova plugin unlock cache : start ....');
    exec(success, error, 'cacheDataSync', 'unlock_cache', [arg0]);
};

// subscriptionResult.result = null;

// module.exports = subscriptionResult();

// module.exports.networkSubscriber = function (arg0, success, error) {
//     console.log('... cordova plugin network subscriber : start ....');
//     SyncPlugin.networkSubscriber({});
//     exec(success, error, 'cacheDataSync', 'network_listener', [arg0]);
// };

// module.exports.subscriberTest = function (arg0, success, error) {
//     console.log('########### NETWORK SUBSCRIBER TRIGGERED ######### -> ', msg);
//     return new Promise((resolve, reject) => {
//         channel.onCordovaReady.subscribe(function(){
//             exec(success, null, 'cacheDataSync', 'network_listener', []);
//
//             function success(msg) {
//                 console.log('########### NETWORK SUBSCRIBER TRIGGERED ######### -> ', msg);
//                 return msg;
//             }
//         });
//     });
// }

// self.addEventListener('message', function (event) {
//     let msg = event.data;
//
// }, false);


channel.onCordovaReady.subscribe(function(){
    exec(success, null, 'cacheDataSync', 'network_listener', []);

    function success(msg) {
        console.log('########### NETWORK SUBSCRIBER TRIGGERED ######### -> ', msg);
        var event = new CustomEvent('EVENT_VALIDATION_RECEIIVED', {detail: msg});
        window.dispatchEvent(event);
        // postMessage('subscription_update');

    }
});

// document.addEventListener()

// module.exports.subscriber = new Observable(subscriber => {
//     console.log('Network Subscriber initialised #######');
//     subscriber.next()
// })

var SyncPlugin = {
    subscribe:function (arg0) {
     //  call to my plugin

        console.log('SQLITE PLUGIN CALLBACK FROM js interface ************** ');
        var instance =  window.sqlitePlugin.openDatabase({name: '_network_cache_storage',location: 'default',androidDatabaseProvider: 'system'});

    exec(function (result) {
        console.log('success callback -> ' , result);
         instance.executeSql(result.query,"",function (res) { },function (err) {console.log('executeSql err -->',err);});
    }, 
    function (err) {
       console.log('error ... ',err);
    }, 'cacheDataSync', 'sqlite_listener', [arg0]);
    }
}
