package syncData;

import android.content.Context;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
/**
 * This class echoes a string called from JavaScript.
 */
public class cacheDataSync extends CordovaPlugin {

    public static final String TAG = "cacheDataSync";
    private CallbackContext sqliteCallback;
    private CallbackContext networkCallback;
    public static String eventServiceHeartBeatUrl = "no_event_service_heart_beat_url";
    public static String badEventSyncUrl = "no_bad_event_sync_url";
    public static int retryCount = 50;
    public boolean syncInProgress = false;
    public boolean cacheLocked = false;
    public String subId = null;


    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("coolMethod")) {
            String message = args.getString(0);
            this.coolMethod(message, callbackContext);
            return true;
        } else if (action.equals("start_sync")) {
            Log.e(TAG, " #### start_sync ####  args : "+args);
            JSONObject args0 = args.getJSONObject(0);
            // Log.e(TAG, " #### start_sync ####  args0 : "+args0);
            eventServiceHeartBeatUrl = args0.getString("event_service_heartbeat_url");
            badEventSyncUrl = args0.getString("bad_event_sync_url");
            retryCount = args0.getInt("retry_count");

            Log.e(TAG, " #### start_sync ####  event_service_heartbeat_url : "+eventServiceHeartBeatUrl);
            Log.e(TAG, " #### start_sync ####  bad_event_sync_url : "+badEventSyncUrl);
            Log.e(TAG, " #### start_sync ####  retry_count : "+retryCount);

            this.start(callbackContext);
        }else if (action.equals("sqlite_listener")) {
            this.registerSqliteListener(callbackContext);
        }else if (action.equals("network_listener")) {
            this.registerNetworkListener(callbackContext);
            Log.e(TAG, " #### NETWORK SUBSCRIPTION LISTENER INIT ###### ");
        } else if (action.equals("lock_cache")) {
            this.lockCache(args, callbackContext);
        } else if (action.equals("unlock_cache")) {
            this.unlockCache(args, callbackContext);
        }
        return true;
    }

    private void coolMethod(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    private void registerSqliteListener(CallbackContext callbackContext) {
        Log.e(TAG, " === registerSqliteListener ===  : "+callbackContext);
        this.sqliteCallback = callbackContext;
    }

    private void registerNetworkListener(CallbackContext callbackContext) {
        Log.e(TAG, " === registerNetworkListener ===  : "+callbackContext);
        this.networkCallback = callbackContext;
    }

    public void sendToPlugin(JSONObject message){

        try {
            Log.e(TAG, message.toString()+" *** cacheDataSync.deleteListener.isFinished ***  - "+this.sqliteCallback.isFinished());
            PluginResult result = new PluginResult(PluginResult.Status.OK, message);
            result.setKeepCallback(true);
//            this.sqliteCallback.success(result);
            this.sqliteCallback.sendPluginResult(result);
        }catch (Exception ex){
            Log.e(TAG, "EXCEPTION : sendToPlugin " + ex.getLocalizedMessage());
        }
    }

    public void sendNetworkSubscriberResult(JSONObject message){

        try {
            Log.e(TAG, message.toString()+" *** network subscription triggered ***  - "+this.networkCallback.isFinished());
            PluginResult result = new PluginResult(PluginResult.Status.OK, message);
            result.setKeepCallback(true);
            this.networkCallback.sendPluginResult(result);
        }catch (Exception ex){
            Log.e(TAG, "EXCEPTION : network subscription -> " + ex.getLocalizedMessage());
        }
    }

    private void start(final CallbackContext callbackContext) {
        Log.e(TAG, "start method");
        final Context context = this.cordova.getActivity().getApplicationContext();
        cordova.getThreadPool().execute(new Sync(this,context));
        callbackContext.success("start success ");
    }

    private void lockCache(JSONArray args, CallbackContext callbackContext) {
        Log.e(TAG, " #### LOCKING CACHE ####  event in progress : "+args);
        /** lock the variable here*/
        if (!syncInProgress) {
            try {
                cacheLocked = true;
                final PluginResult result = new PluginResult(PluginResult.Status.OK, "success");
                callbackContext.sendPluginResult(result);
                Log.e(TAG, " #### LOCK CACHE SUCCESS #### ");
            } catch (Exception e) {
                cacheLocked = false;
                Log.e(TAG, " #### ERROR LOCKING CACHE ####  could not lock cache : "+args);
            }
        } else {
            Log.e(TAG, " #### ERROR LOCKING CACHE ####  could not lock cache : "+args);
            final PluginResult result = new PluginResult(PluginResult.Status.OK, "failed");
            callbackContext.sendPluginResult(result);
        }
    }

    private void unlockCache(JSONArray args, CallbackContext callbackContext) {
        Log.e(TAG, " #### UNLOCKING CACHE ####  event ended : "+args);
        /** unlock the variable here*/
        cacheLocked = false;
        final PluginResult result = new PluginResult(PluginResult.Status.OK, "success");
        callbackContext.sendPluginResult(result);
    }
}

class Sync implements Runnable{

    public boolean SYNC_BAD_RECORD_IN_PROGRESS = false;
    private syncData.DatabaseService dbs = null;
    private cacheDataSync cacheDataSync;
    private Context context;

    LinkedList<String> successMessageKeyList = new LinkedList<String>();

    public Sync(syncData.cacheDataSync cacheDataSync,Context context) {
        this.cacheDataSync = cacheDataSync;
        this.context = context;
//        dbs = new syncData.DatabaseService(context);
    }

    public void run() {

        dbs = new syncData.DatabaseService(context);
        syncData.DatabaseService.initializeInstance(context);
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            public void run() {
                try {
                    boolean networkStatus = checkNetworkStatus(syncData.cacheDataSync.eventServiceHeartBeatUrl);
                    Log.e(syncData.cacheDataSync.TAG, "checkNetworkStatus() : " + networkStatus);

                    if (networkStatus) {
                        Log.e(cacheDataSync.TAG, "*** Start Syncing Cache *** ");
                        startSyncNetworkCache();
                        startSyncBadRecords();
                    }
                }catch (Exception e){
                    Log.e(cacheDataSync.TAG, "*** Exception *** "+ e.getMessage());
                    sendErrorsToBackend(e.getMessage());
                }
            }
        };
        timer.schedule(timerTask, 10, 60 *1000 );//	delay the task 10 second, and then run task every min
    }

    private void sendErrorsToBackend(String message) {
        try {
            JSONObject request = new JSONObject();
            request.put("deviceId", this.cacheDataSync.subId);
            request.put("message", message);

            URL url = new URL("https://nfactorylive.ncinga.com/app_licence_services/devicestatus/v1/errors");
            HttpURLConnection httpsURLConnection = (HttpURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("POST");

            httpsURLConnection.setRequestProperty("Content-Type", "application/json");

            httpsURLConnection.setDoOutput(true);
            DataOutputStream dataOutputStream = new DataOutputStream(httpsURLConnection.getOutputStream());
            dataOutputStream.writeChars(request.toString());
            dataOutputStream.flush();
            dataOutputStream.close();

            InputStream in = new BufferedInputStream(httpsURLConnection.getInputStream());
            String result = readStream(in);
            Log.e(cacheDataSync.TAG, "*** SAVE LOGS REsULT *** "+ result);

        } catch (Exception e) {
            Log.e(cacheDataSync.TAG, "*** Exception *** "+ e.getMessage());
        }
    }

    private void startSyncNetworkCache() throws Exception {

        Log.e(cacheDataSync.TAG, " === startSyncNetworkCache === 1.0.21 '=== cacheDataSync.syncInProgress : "+cacheDataSync.syncInProgress);
        Log.e(cacheDataSync.TAG, " === Test Log for new version === 1.0.22 '=== SUCCESS : ");
        if (!cacheDataSync.syncInProgress && !cacheDataSync.cacheLocked) {
            try {
                cacheDataSync.syncInProgress = true;
                this.printDataList();
                this.syncNetworkCache(0);
            } catch (Exception e) {
                cacheDataSync.syncInProgress = false;
                Log.e(cacheDataSync.TAG, " === EXCEPTION === : "+e.getMessage());
                throw e;
            }
        } else {
            Log.e(cacheDataSync.TAG, " #### CACHE SYNCING FAILED - cache locked ! #### ");
        }

    }

    private void syncNetworkCache(int index) throws JSONException, Exception {
//        cacheDataSync.syncInProgress = true;
        int successCount = 0;

        try{
            ArrayList<JSONObject> defaultCallList = new ArrayList();
            ArrayList<JSONObject> eventList = new ArrayList();

            ArrayList<JSONObject> failNetworkCalls = dbs.getFailNetworkCals(index);

//        boolean networkStatus = checkNetworkStatus(syncData.cacheDataSync.eventServiceHeartBeatUrl);
//        Log.e(syncData.cacheDataSync.TAG, "checkNetworkStatus() : " + networkStatus);

            if (failNetworkCalls.size()>0) {
                for (JSONObject failNetworkCall : failNetworkCalls) {

                    Log.e(cacheDataSync.TAG, " === failNetworkCall === " + failNetworkCall + " ===");

                    if (!this.isInSuccessMessageKeyList(failNetworkCall.getString("_key"))) {

                        //SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        long lastUpdateTime = failNetworkCall.getLong("last_update_time");

                        //parser.setTimeZone(TimeZone.getTimeZone("UTC"));
                        long timeGap = new Date().getTime() - lastUpdateTime;

                        Log.e(cacheDataSync.TAG, " === TIME GAP === " + timeGap + " ===");
                        Log.e(cacheDataSync.TAG, " === STATUS === " + failNetworkCall.getString("status") + " ===");

                        if (failNetworkCall.getString("status").equalsIgnoreCase("fail") || timeGap > 5 * 60 * 1000) {
                            String messageType = failNetworkCall.getString("message_type");
                            if (messageType.equalsIgnoreCase("event")) {
                                eventList.add(failNetworkCall);
                            } else {
                                defaultCallList.add(failNetworkCall);
                            }
                            //dbs.updateNetworkCallStatus(failNetworkCall, "in-progress");
//                        this.updateNetworkCallStatus(failNetworkCall, "in-progress");
                        }
                    }

                }

                boolean successDefaultCalls = this.sendFailDefaultCallListNew(defaultCallList);
                boolean successEvents = this.sendFailEventsNew(eventList);


                Log.e(syncData.cacheDataSync.TAG, "successMessageKeyList : " + this.successMessageKeyList);

                if (successEvents || successDefaultCalls) {

                    Log.e(cacheDataSync.TAG, "##############    Next batch Syncing  From Beginning  ##########");
                    this.syncNetworkCache(0);

                } else {
                    Log.e(cacheDataSync.TAG, "##############    Next batch Syncing  From last index  ##########");
                    int indexNew = index + failNetworkCalls.size();
                    this.syncNetworkCache(indexNew);
//                    cacheDataSync.syncInProgress = false;
                }
            }
            cacheDataSync.syncInProgress = false;
        } catch (Exception e){
            cacheDataSync.syncInProgress = false;
            Log.e(cacheDataSync.TAG, " === EXCEPTION === : " + e.getMessage());
            throw e;
        }
    }

    private void startSyncBadRecords() throws Exception {

        Log.e(cacheDataSync.TAG, " === startSyncBadRecords cacheDataSync.syncInProgress : "+SYNC_BAD_RECORD_IN_PROGRESS);
        Log.e(cacheDataSync.TAG, " === Test Log for new version === 1.0.22 '=== SUCCESS : ");
        if (!SYNC_BAD_RECORD_IN_PROGRESS) {
            try {
                this.syncBadRecords();
            } catch (Exception e) {
                Log.e(cacheDataSync.TAG, " === EXCEPTION === : "+e.getMessage());
                SYNC_BAD_RECORD_IN_PROGRESS = false;
                throw e;
            }
        }

    }

    private void syncBadRecords() throws JSONException, Exception {

        SYNC_BAD_RECORD_IN_PROGRESS = true;

        try {
            ArrayList<JSONObject> badRecords = dbs.getBadRecords();
            if (badRecords.size()>0) {

                boolean status = this.sendBadRecords(badRecords);

                if (status) {
                    try {
//                    Thread.sleep(15000);
                        this.syncBadRecords();
                    } catch (Exception e){
                        Log.e(cacheDataSync.TAG, " === EXCEPTION Thread sleep=== : " + e.getMessage());
                        SYNC_BAD_RECORD_IN_PROGRESS = false;
                    }

                } else {
                    SYNC_BAD_RECORD_IN_PROGRESS = false;
                }
            }else {
                SYNC_BAD_RECORD_IN_PROGRESS = false;
            }
        } catch (Exception e) {
            Log.e(cacheDataSync.TAG, " === EXCEPTION ===" + e);
            throw e;
        }
    }

    private boolean sendBadRecords(ArrayList<JSONObject> badRecords) throws JSONException, Exception {
        JSONObject request = new JSONObject();
        request.put("records",badRecords);

        Log.e(cacheDataSync.TAG, " === sendBadRecords === request : " + request);

        try {
            URL url = new URL(syncData.cacheDataSync.badEventSyncUrl);
            HttpURLConnection httpsURLConnection = (HttpURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("POST");

            httpsURLConnection.setRequestProperty("Content-Type", "application/json");

            httpsURLConnection.setDoOutput(true);
            DataOutputStream dataOutputStream = new DataOutputStream(httpsURLConnection.getOutputStream());
            dataOutputStream.writeChars(request.toString());
            dataOutputStream.flush();
            dataOutputStream.close();

            InputStream in = new BufferedInputStream(httpsURLConnection.getInputStream());
            String result = readStream(in);

            JSONObject jsonResponse = new JSONObject(result);
            if (jsonResponse != null && jsonResponse.get("status").equals("success")){
                return this.removeNetworkRecordNew(badRecords);
            }
        } catch (Exception e) {
            Log.e(cacheDataSync.TAG, " === EXCEPTION ===" + e);
            e.printStackTrace();
            throw e;
        }
        return false;
    }


//    private int sendFailDefaultCallList(ArrayList<JSONObject> defaultCallList) throws JSONException {
//        int successCount = 0;
//        boolean responseStatus = false;
//
//        if (defaultCallList.size() > 0) {
//            for (JSONObject networkCall : defaultCallList) {
//                responseStatus = this.sendPost(networkCall.getString("url"), networkCall.getJSONObject("message"), networkCall.getString("message_type"), networkCall.getJSONObject("message_header"), null, networkCall);
//                if (responseStatus) {
//                    successCount++;
//                }
//            }
//        }
//
//        return successCount;
//    }

    private boolean sendFailDefaultCallListNew(ArrayList<JSONObject> defaultCallList) throws JSONException, Exception {
        int successCount = 0;
        boolean responseStatus = false;

        try {
            if (defaultCallList.size() > 0) {
                for (JSONObject networkCall : defaultCallList) {
                    responseStatus = this.sendPost(networkCall.getString("url"), networkCall.getJSONObject("message"), networkCall.getString("message_type"), networkCall.getJSONObject("message_header"), null, networkCall);
                    if (responseStatus) {
                        successCount++;
                    }
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e(cacheDataSync.TAG, " === EXCEPTION ===" + e);
            throw e;
        }
        return successCount == defaultCallList.size();
    }

//    private int sendFailEvents(ArrayList<JSONObject> eventList) throws JSONException {
//        int successCount = 0;
//        boolean responseStatus = false;
//        Date sendtime = new Date();
//
//        if (eventList.size() > 0) {
//            JSONArray records = new JSONArray();
//            String url = "";
//            JSONObject headers = new JSONObject();
//            JSONObject msg = new JSONObject();
//
//            for (JSONObject event : eventList) {
//
//                JSONObject record = new JSONObject();
//
//                JSONObject message = (JSONObject) event.get("message");
//                JSONObject w1 = (JSONObject) message.get("w1");
//                JSONObject cc = (JSONObject) w1.get("cc");
//                cc.put("o", sendtime.getTime());
//                w1.put("cc", cc);
//                message.put("w1", w1);
//
//                record.put("value", message);
//                record.put("key", String.valueOf(this.getRandom())); //use this key to select partition
//                url = event.getString("url");
//
//                headers = event.getJSONObject("message_header");
//                records.put(record);
//            }
//
//            msg.put("records", records);
//
//            Log.e(cacheDataSync.TAG, " === startSyncNetworkCache sendPost === msg ===" + msg);
//            responseStatus = this.sendPost(url, msg, "event", headers, eventList, null);
//            if (responseStatus) {
//                successCount++;
//            }
//        }
//
//        return successCount;
//    }

    private boolean sendFailEventsNew(ArrayList<JSONObject> eventList) throws JSONException, Exception {
        boolean responseStatus = false;
        Date sendtime = new Date();

        try {
            if (eventList.size() > 0) {
                JSONArray records = new JSONArray();
                String url = "";
                String subjectKey = "";
                boolean serverValidation = false;
                JSONObject headers = new JSONObject();
                JSONObject msg = new JSONObject();

                for (JSONObject event : eventList) {

                    JSONObject record = new JSONObject();

                    JSONObject message = (JSONObject) event.get("message");
                    if (event.getString("sk") != null) {
                        subjectKey =  event.getString("sk");
                    }
                    serverValidation = event.getBoolean("server_validation");
                    JSONObject w1 = (JSONObject) message.get("w1");
                    JSONObject cc = (JSONObject) w1.get("cc");

                    if (this.cacheDataSync.subId == null) {
                        JSONObject a = (JSONObject) message.get("a");
                        String subId =  a.getString("sub");
                        this.cacheDataSync.subId = subId;
                    }

                    cc.put("o", sendtime.getTime());
                    w1.put("cc", cc);
                    message.put("w1", w1);

                    record.put("value", message);
                    record.put("key", String.valueOf(this.getRandom())); //use this key to select partition
                    url = event.getString("url");

                    headers = event.getJSONObject("message_header");
                    records.put(record);
                }

                msg.put("records", records);
                msg.put("subject_key", subjectKey);
                msg.put("server_validation", serverValidation);
                msg.put("datetime", sendtime.getTime());

                Log.e(cacheDataSync.TAG, " === startSyncNetworkCache sendPost === msg ===" + msg);
                return this.sendPost(url, msg, "event", headers, eventList, null);
            } else {
                return false;
            }
        } catch (Exception e){
            Log.e(cacheDataSync.TAG, " === EXCEPTION ===" + e);
            throw e;
        }
    }

    private int getRandom(){
        return new Random().nextInt(4)+1;
    }

    private void printDataList() {
        dbs.printAllData();
    }


    private boolean sendPost(String urlStr, JSONObject msg, String messageType, JSONObject headers, ArrayList<JSONObject> dataArray, JSONObject data) throws JSONException, Exception {

        try {
            URL url = new URL(urlStr);
            HttpURLConnection httpsURLConnection = (HttpURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("POST");

            Iterator<String> iterator = headers.keys();

            while (iterator.hasNext()) {
                String key = iterator.next();
                httpsURLConnection.setRequestProperty(key, headers.getString(key));
            }

            httpsURLConnection.setDoOutput(true);
            DataOutputStream dataOutputStream = new DataOutputStream(httpsURLConnection.getOutputStream());
            dataOutputStream.writeChars(msg.toString());
            dataOutputStream.flush();
            dataOutputStream.close();

            InputStream in = new BufferedInputStream(httpsURLConnection.getInputStream());
            String result = readStream(in);

            JSONObject jsonResponse = new JSONObject(result);
            ///////////////

            this.cacheDataSync.sendNetworkSubscriberResult(jsonResponse);

            ////////////
            return this.processResponse(jsonResponse, msg, messageType, dataArray, data);

        } catch (Exception e) {
            Log.e(cacheDataSync.TAG, " === EXCEPTION ===" + e);
            throw e;
        }

    }


    private boolean checkNetworkStatus(String urlStr) {

        try {
            URL url = new URL(urlStr);
            HttpURLConnection httpsURLConnection = (HttpURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("GET");

            if (httpsURLConnection.getResponseCode() == 200){
                //InputStream in = new BufferedInputStream(httpsURLConnection.getInputStream());
                //String result = readStream(in);
                return true;
            }

            return false;
        } catch (MalformedURLException e) {
            Log.e(cacheDataSync.TAG, " === jsonResponse === 1 ==="+" URL ["+urlStr+"]" + e);
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            Log.e(cacheDataSync.TAG, " === jsonResponse === 2 ==="+" URL ["+urlStr+"]" + e);
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            Log.e(cacheDataSync.TAG, " === jsonResponse === 3 ==="+" URL ["+urlStr+"]" + e);
            e.printStackTrace();
            return false;
        }

    }

    private String readStream(InputStream is) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int i = is.read();
            while (i != -1) {
                bo.write(i);
                i = is.read();
            }
            return bo.toString();
        } catch (IOException e) {
            return "";
        }
    }


    private boolean processResponse(JSONObject response, JSONObject msg, String messageType, ArrayList<JSONObject> dataArray, JSONObject data) throws JSONException, Exception {
        // Log.e(TAG, " === ProcessResponse : response==="+response);
        // Log.e(TAG, " === ProcessResponse : data ==="+data);
        // Log.e(TAG, " === ProcessResponse : messageType ==="+messageType);

        try{
            if (messageType.equalsIgnoreCase("event")) {
                return this.processEventResponse(response, msg, dataArray);
            } else {
                return this.processDefaultResponse(response, msg, data);
            }
        } catch (Exception e) {
            Log.e(cacheDataSync.TAG, " === EXCEPTION ===" + e);
            throw e;
        }

    }

    private boolean processEventResponse(JSONObject response, JSONObject msg, ArrayList<JSONObject> dataArray) throws JSONException, Exception {

        try {
            // TODO - Improve the validation here after Finishing Model 3 changes
            Log.e(cacheDataSync.TAG, " === Event Hub Response -> " + response);
            if (response != null && response.get("records") != null) {
                JSONArray offsets = response.getJSONArray("records");
                JSONObject offset = offsets.getJSONObject(0);

                if (offset != null) {
                    // remove event list
                    Log.e(cacheDataSync.TAG, " === processEventResponse : success ===");
                    return this.removeNetworkRecordNew(dataArray);
                } else {
                    // fail event list
                    Log.e(cacheDataSync.TAG, " === processEventResponse : ERR ===");
                    this.updateEventListStatusNew(dataArray, "fail");
                    return false;
                }
            } else {
                Log.e(cacheDataSync.TAG, " === processDefaultResponse : offset = null ===" + msg);
                // fail event list
                Log.e(cacheDataSync.TAG, " === processEventResponse : ERR ===");
                this.updateEventListStatusNew(dataArray, "fail");
                return false;
            }
        } catch (Exception e){
            Log.e(cacheDataSync.TAG, " === EXCEPTION ===" + e);
            throw e;
        }
    }

    private boolean processDefaultResponse(JSONObject response, JSONObject msg, JSONObject data) throws JSONException {

        Log.e(cacheDataSync.TAG, " === processDefaultResponse : response ===" + response);
        int deleteCount = 0;

        try{
            if (response != null && response.getString("msg_id") != null) {
                if (response.getString("acknowledgement").equalsIgnoreCase("success")) {
                    // dbs.removeNetworkRecords(response.getString("msg_id"));
                    String key = response.getString("msg_id");
                    this.addToSuccessMessageKeyList(key);
                    Log.e(syncData.cacheDataSync.TAG, "removeNetworkRecords key : " + key);
//                this.removeNetworkRecord(response.getString("msg_id"));
                    deleteCount = this.dbs.deleteNetworkCall(key);
//                return true;

                } else {
                    // dbs.updateNetworkCallStatus(data, "fail");
                    this.updateNetworkCallStatusNew(data,"fail");
//                return false;
                }
            } else {
                //dbs.updateNetworkCallStatus(data, "fail");
                this.updateNetworkCallStatusNew(data,"fail");
//            return false;
            }
        } catch (Exception e){
            Log.e(cacheDataSync.TAG, " === EXCEPTION ===" + e);
            throw e;
        }
        return deleteCount > 0;
    }

    private void updateEventListStatus(ArrayList<JSONObject> dataArray, String status) throws JSONException {

        for (JSONObject data : dataArray) {
            if (status.equalsIgnoreCase("remove")) {
                Log.e(cacheDataSync.TAG, " === updateEventListStatus : remove ===value : " + data);
                //dbs.removeNetworkRecords(data.getString("id"));
                this.removeNetworkRecord(data.getString("id"));
            } else {
                //dbs.updateNetworkCallStatus(data, status);
                this.updateNetworkCallStatus(data,status);
            }
        }
    }

    private boolean updateEventListStatusNew(ArrayList<JSONObject> dataArray, String status) throws JSONException {
        int successCount = 0;

        try {
            for (JSONObject data : dataArray) {

                if (status.equalsIgnoreCase("fail")){
                    int failCount = data.has("fail_count")?data.getInt("fail_count"):0;
                    failCount++;
                    data.put("fail_count",failCount);
                }
                String key = data.getString("id");
                Log.e(syncData.cacheDataSync.TAG,"updateNetworkCallStatus key : "+ key +" status : "+status+" value : "+data);

                data.put("status",status);
                data.put("last_update_time",new Date().getTime());

//                String query = "UPDATE network_cache SET value = '"+String.valueOf(data)+"' WHERE key = '"+key+"'";

                successCount += this.dbs.updateNetworkCall(key, String.valueOf(data));
            }
        } catch (Exception e){
            Log.e(cacheDataSync.TAG, " === EXCEPTION ===" + e);
            throw e;
        }

        return successCount == dataArray.size();
    }

    public boolean removeNetworkRecordNew(ArrayList<JSONObject> dataArray) throws JSONException, Exception {
        int successCount = 0;

        try {
            for (JSONObject data : dataArray) {
                String key = data.getString("id");
                this.addToSuccessMessageKeyList(key);
                Log.e(syncData.cacheDataSync.TAG, "removeNetworkRecords key : " + key);
                successCount += this.dbs.deleteNetworkCall(key);
            }
            return successCount == dataArray.size();
        } catch (Exception e){
            Log.e(cacheDataSync.TAG, " === EXCEPTION ===" + e);
            throw e;
        }
    }


    public void removeNetworkRecord(String key){
        try {
            this.addToSuccessMessageKeyList(key);
            Log.e(syncData.cacheDataSync.TAG, "removeNetworkRecords key : " + key);
            String query = "DELETE FROM network_cache WHERE key = '"+key+"'";
            JSONObject message = new JSONObject();
            message.put("query", query);
            this.cacheDataSync.sendToPlugin(message);
        }catch (Exception ex){
            Log.e(syncData.cacheDataSync.TAG, "EXCEPTION : removeNetworkRecords -" + ex.getLocalizedMessage());
        }
    }

    public void updateNetworkCallStatus(JSONObject value,String status){
        try {
            if (status.equalsIgnoreCase("fail")){
                int failCount = value.has("fail_count")?value.getInt("fail_count"):0;
                failCount++;
                value.put("fail_count",failCount);
            }
            String key = value.getString("id");
            Log.e(syncData.cacheDataSync.TAG,"updateNetworkCallStatus key : "+ key +" status : "+status+" value : "+value);

            value.put("status",status);
            value.put("last_update_time",new Date().getTime());

            String query = "UPDATE network_cache SET value = '"+String.valueOf(value)+"' WHERE key = '"+key+"'";

            JSONObject message = new JSONObject();
            message.put("query", query);
            this.cacheDataSync.sendToPlugin(message);

        }catch (Exception e){
            Log.e(syncData.cacheDataSync.TAG,"updateNetworkCallStatus Exception : "+ e.getMessage());
        }
    }

    public int updateNetworkCallStatusNew(JSONObject value,String status){
        try {
            if (status.equalsIgnoreCase("fail")){
                int failCount = value.has("fail_count")?value.getInt("fail_count"):0;
                failCount++;
                value.put("fail_count",failCount);
            }
            String key = value.getString("id");
            Log.e(syncData.cacheDataSync.TAG,"updateNetworkCallStatus key : "+ key +" status : "+status+" value : "+value);

            value.put("status",status);
            value.put("last_update_time",new Date().getTime());

            return this.dbs.updateNetworkCall(key, String.valueOf(value));

//            String query = "UPDATE network_cache SET value = '"+String.valueOf(value)+"' WHERE key = '"+key+"'";
//            UPDATE network_cache SET 1612849467544-1103=? WHERE key LIKE ?
//
//            JSONObject message = new JSONObject();
//            message.put("query", query);
//            this.cacheDataSync.sendToPlugin(message);

        }catch (Exception e){
            Log.e(syncData.cacheDataSync.TAG,"updateNetworkCallStatus Exception : "+ e.getMessage());
            return 0;
        }
    }


    private void addToSuccessMessageKeyList(String key){
        if (this.successMessageKeyList.size()>200){
            this.successMessageKeyList.removeFirst();
        }
        this.successMessageKeyList.addLast(key);

    }

    private boolean isInSuccessMessageKeyList(String key){
        return this.successMessageKeyList.contains(key);
    }

}

