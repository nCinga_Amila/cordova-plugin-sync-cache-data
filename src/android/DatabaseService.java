package syncData;


import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONException;
import org.json.JSONObject;

//import javax.naming.Context;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.ArrayList;


/**
 * This class echoes a string called from JavaScript.
 */
public class DatabaseService extends SQLiteOpenHelper {
    private static final String TAG = "cacheDataSync";

    private static final String DB_NAME = "_network_cache_storage";
    private static final String TABLE_NAME = "network_cache";
    private static final String COL1 = "id";
    private static final String COL2 = "key";
    private static final String COL3 = "value";
    private AtomicInteger mOpenCounter = new AtomicInteger();
    //private SQLiteDatabase db;


    private static DatabaseService instance;
//    private static SQLiteOpenHelper mDatabaseHelper;
    private SQLiteDatabase mDatabase;

    public static synchronized void initializeInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseService(context);
//            mDatabaseHelper = helper;
        }
    }

    public static synchronized DatabaseService getInstance() {
        if (instance == null) {
            throw new IllegalStateException(DatabaseService.class.getSimpleName() +
                    " is not initialized, call initializeInstance(..) method first.");
        }

        return instance;
    }

    public synchronized SQLiteDatabase openDatabase() {
        if(mOpenCounter.incrementAndGet() == 1) {
            // Opening new database
            mDatabase = this.getWritableDatabase();
        }
        return mDatabase;
    }

    public synchronized void closeDatabase() {
        if(mOpenCounter.decrementAndGet() == 0) {
            // Closing database
            mDatabase.close();

        }
    }

    public DatabaseService(Context context) {
        super(context, DB_NAME,null,1);
        //this.db = this.getWritableDatabase();
        Log.e(TAG, "=== DatabaseService : Constructor ===");

    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL("");
        Log.e(TAG, "=== DatabaseService : onCreate ===");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public void printAllData(){

//        SQLiteDatabase database = this.getReadableDatabase();
        SQLiteDatabase database = DatabaseService.getInstance().openDatabase();
        Cursor res = database.rawQuery("SELECT * FROM "+TABLE_NAME, null);
        //Cursor res = database.rawQuery("SELECT * FROM "+TABLE_NAME + " ORDER BY key DESC LIMIT 10", null);

        Log.e(TAG, "################################### Print All Data ###################################");
        while (res.moveToNext()){

            Log.e(TAG, "--------------------------------------------------------------------------- : ");
            Log.e(TAG, res.getColumnName(0) + " : " + res.getString(0));
            Log.e(TAG, res.getColumnName(1) + " : " + res.getString(1));
            Log.e(TAG, res.getColumnName(2) + " : " + res.getString(2));
            Log.e(TAG, "--------------------------------------------------------------------------- : ");
        }
        res.close();
        DatabaseService.getInstance().closeDatabase();
//        database.close();

        Log.e(TAG, "#######################################################################################");
    }

    public int deleteNetworkCall(String key) {
        try {
            SQLiteDatabase database = DatabaseService.getInstance().openDatabase();
            String selection = "key LIKE ?";
            String[] selectionArgs = { key };
            int deleteRows = database.delete(TABLE_NAME, selection, selectionArgs);
            Log.e(TAG, "SQLITE DATABASE DELETE RESULT -> " + deleteRows);
            DatabaseService.getInstance().closeDatabase();
            if (deleteRows > 0) {
                return deleteRows;
            } else {
                return 0;
            }
        } catch (Exception e) {
            Log.e(TAG, "EXCEPTION : deleteNetworkCalls : "+e.getMessage());
            DatabaseService.getInstance().closeDatabase();
            throw e;
        }
    }

    public int updateNetworkCall(String key, String value) {
        try {
            SQLiteDatabase database = DatabaseService.getInstance().openDatabase();
            ContentValues values = new ContentValues();
            values.put("value", value);
            String selection = "key LIKE ?";
            String[] selectionArgs = { key };
            int updateRows = database.update(TABLE_NAME, values, selection, selectionArgs);
            Log.e(TAG, "SQLITE DATABASE UPDATE RESULT -> " + updateRows);
            DatabaseService.getInstance().closeDatabase();
            if (updateRows > 0) {
                return updateRows;
            } else {
                return 0;
            }
        } catch (Exception e) {
            Log.e(TAG, "EXCEPTION : updateNetworkCalls : "+e.getMessage());
            DatabaseService.getInstance().closeDatabase();
            throw e;
        }
    }

    public ArrayList<JSONObject> getFailNetworkCals(int skip){
        ArrayList<JSONObject> failNetworkCalls = new ArrayList();

        try{
            SQLiteDatabase database = DatabaseService.getInstance().openDatabase();

            Cursor res = database.rawQuery("SELECT * FROM "+TABLE_NAME +" ORDER BY key ASC LIMIT "+skip+", 10", null);
            if (res.getCount() > 0) {
                while (res.moveToNext()) {
                    String dataStr = res.getString(2);
                    String key = res.getString(1);
                    JSONObject data = new JSONObject(dataStr);
                    data.put("_key", key);
                    int failCount = data.has("fail_count") ? data.getInt("fail_count") : 0;
                    if (failCount <= syncData.cacheDataSync.retryCount) {
                        failNetworkCalls.add(data);
                    }
                }

                if (res != null) {
                    res.close();
                }
                DatabaseService.getInstance().closeDatabase();

                // if there is no data in the cache database. this method call is never end. that's why we check is query result empty
                if (failNetworkCalls.isEmpty()) {
                    return getFailNetworkCals(skip + 10);
                }
            } else {
                if (res != null) {
                    res.close();
                }
                DatabaseService.getInstance().closeDatabase();
            }
        } catch (JSONException e) {
            Log.e(TAG, "EXCEPTION : getFailNetworkCals : "+e.getMessage());
        } catch (Exception e){
            Log.e(TAG, "EXCEPTION : getFailNetworkCals : " + e.getMessage());
            throw e;
        }

        return failNetworkCalls;
    }


    public ArrayList<JSONObject> getBadRecords(){
        ArrayList<JSONObject> badRecords = new ArrayList();
        SQLiteDatabase database = DatabaseService.getInstance().openDatabase();

        Cursor res = database.rawQuery("SELECT * FROM "+TABLE_NAME +" ORDER BY key ASC LIMIT 10", null);
        while (res.moveToNext()) {
            String dataStr = res.getString(2);
            String key = res.getString(1);
            try {
                JSONObject data = new JSONObject(dataStr);
                data.put("_key",key);
                int failCount = data.has("fail_count")?data.getInt("fail_count"):0;
                if (failCount > syncData.cacheDataSync.retryCount){
                    badRecords.add(data);
                }

            } catch (JSONException e) {
                Log.e(TAG, "EXCEPTION : getFailNetworkCals : "+e.getMessage());
            }
        }

        if (res!=null){
            res.close();
        }
        DatabaseService.getInstance().closeDatabase();

        return badRecords;
    }


}